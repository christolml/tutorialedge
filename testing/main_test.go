package main

import "testing"

type AddResult struct {
	x int
	y int
	expected int
}


var addResults = []AddResult{
	{1,1,2},
}

// primera forma de hacer testing, especificando las entradas y salidas esperadas

func TestAdd(t *testing.T) {
	for _, test := range addResults {
		result := Add(test.x, test.y)
		if result != test.expected {
			t.Fatal("Expected result not given")
		}
	}

}

// segunda forma de hacer testing a un directorio

func TestReadFile(t *testing.T) {

}
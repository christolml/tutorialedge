package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"net/http"
)

var db *gorm.DB
var err error

type User struct {
	gorm.Model
	Name    string
	Email   string
}

func InitialMigration(){
	db := GetConnection()
	
	defer db.Close()
	
	db.AutoMigrate(&User{})
}


func AllUsers(w http.ResponseWriter, r *http.Request){
	db := GetConnection()
	
	defer db.Close()
	
	var users []User
	db.Find(&users)
	json.NewEncoder(w).Encode(users)
	
}

func NewUser(w http.ResponseWriter, r *http.Request){
	db := GetConnection()
	
	defer db.Close()
	
	// con mux.Vars() me esta agarrando los parametros que le estoy enviando a mi ruta por medio de la r que es request
	vars := mux.Vars(r)
	name := vars["name"]
	email := vars["email"]
	
	db.Create(&User{Name:name, Email:email})
	
	fmt.Fprintf(w,"New User Successfully Created")

}

func DeleteUser(w http.ResponseWriter, r *http.Request)  {
	db := GetConnection()
	
	defer db.Close()
	
	vars := mux.Vars(r)
	name := vars["name"]
	
	var user User
	// esta quuery busca en la bd el usuario con el mismo nombre y una vez encontrado la informacion de ese usuario de la bd
	// se lo asigna a nuestra variable user que declaramos aqui
	db.Where("name =?",name).Find(&user)
	// aqui elimina de nuestra base de datos el usuario que le pasamos como parametro el cual fue encontrado en la query anterior
	db.Delete(&user)
	
	fmt.Fprintf(w,"User Successfully Deleted")

}

func UpdateUser(w http.ResponseWriter, r *http.Request)  {
	db := GetConnection()
	
	defer db.Close()
	
	vars := mux.Vars(r)
	name := vars["name"]
	email := vars["email"]
	
	var user User
	db.Where("name =?",name).Find(&user)
	
	user.Email = email
	
	db.Save(&user)
	fmt.Fprintf(w,"User Successfully Updated")

}
